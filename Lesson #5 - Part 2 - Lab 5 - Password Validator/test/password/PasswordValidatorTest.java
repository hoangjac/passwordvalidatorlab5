/*
 * @author Jack Hoang
 * Student Number: 991495093 
 * 
 *  Used as JUnit class to test methods from base class
 *  Used for testing exercise with JUnit
 */

package password;

import static org.junit.Assert.*;

import org.junit.Test;


public class PasswordValidatorTest {

	@Test
	public void testHasValidCaseCharsRegular() {
		boolean result = PasswordValidator.hasValidChar("HelloWorld");
		assertTrue("password does not have valid chars", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionalUpper() {
		boolean result = PasswordValidator.hasValidChar("UPPER");
		assertFalse("password does not have valid chars", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionalSpecial() {
		boolean result = PasswordValidator.hasValidChar("$#%$#%$#%$#");
		assertFalse("password does not have valid chars", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionalBlank() {
		boolean result = PasswordValidator.hasValidChar("");
		assertFalse("password does not have valid chars", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionalNumbers() {
		boolean result = PasswordValidator.hasValidChar("12345653");
		assertFalse("password does not have valid chars", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionalNull() {
		boolean result = PasswordValidator.hasValidChar(null);
		assertFalse("password does not have valid chars", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		boolean result = PasswordValidator.hasValidChar("Pw");
		assertTrue("password does not have valid chars", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOut() {
		boolean result = PasswordValidator.hasValidChar("a123454");
		assertFalse("password does not have valid chars", result);
	}
	
	@Test
	public void testIsValidlengthRegular() {
		boolean result = PasswordValidator.isValidLength("abcdeef12");
		assertTrue("password does not meet length requirement", result);
	}
	
	@Test
	public void testIsValidlengthException() {
		boolean result = PasswordValidator.isValidLength("");
		assertFalse("password does not meet length requirement", result);
	}
	
	@Test
	public void testIsValidlengthBoundaryIn() {
		boolean result = PasswordValidator.isValidLength("test1234");
		assertTrue("password does not meet length requirement", result);
	}
	
	@Test
	public void testIsValidlengthBoundaryOut() {
		boolean result = PasswordValidator.isValidLength("test123");
		assertFalse("password does not meet length requirement", result);
	}

	@Test
	public void testIsValidDigitsRegular() {
		boolean result = PasswordValidator.isValidDigits("test1234");
		assertTrue("password does not contain valid amount of digits", result);
	}
	
	@Test
	public void testIsValidDigitsException() {
		boolean result = PasswordValidator.isValidDigits("test");
		assertFalse("password does not contain valid amount of digits", result);
	}
	
	@Test
	public void testIsValidDigitsBoundaryIn() {
		boolean result = PasswordValidator.isValidDigits("tester12");
		assertTrue("password does not contain valid amount of digits", result);
	}
	
	@Test
	public void testIsValidDigitsBoundaryOut() {
		boolean result = PasswordValidator.isValidDigits("test1");
		assertFalse("password does not contain valid amount of digits", result);
	}

}
